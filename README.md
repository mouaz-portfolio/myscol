# Projet MyScol - Site Web ~ Mouaz MOHAMED, Sofiane BOUHOURIA & Naïm AISSIOUI
Notre projet est un site Web dynamique développé en HTML, CSS, PHP et JavaScript par Mouaz, Sofiane et Naïm durant notre stage chez TiqTec, donné par le directeur de stage Mujeebur RAHAMAN. Ce site permet de se connecter au site à l'aide d'une base de données développée en SQL qui est relié au site Web grâce au PHP et avec le pilote MySQLi, puis par la suite d'avoir accès à trois applications Web : MyTodo™ pour gérer les tâches, MyNotes™ pour prendre des notes et MyPomodoro™ pour gérer son temps via la méthode Pomodoro (25 minutes de travail et 5 minutes de repos).

---

## Diagramme des cas d'utilisation (User Case Diagram) de notre site Web dynamique
```plantuml
left to right direction
:Utilisateur: as Utilisateur
package <uc>MyScol{
    Utilisateur --> (Se connecter au site)
    (Se connecter au site) --> (MyTodo™)
    (Se connecter au site) --> (MyNotes™)
    (Se connecter au site) --> (MyPomodoro™)
    (MyTodo™) ..> (Écrire une tâche)
    (MyTodo™) ..> (Ajouter une tâche)
    (MyTodo™) ..> (Terminer une tâche)
    (MyTodo™) ..> (Supprimer une tâche)
    (MyNotes™) ..> (Écrire le nom du fichier texte .txt)
    (MyNotes™) ..> (Écrire une note)
    (MyNotes™) ..> (Télécharger la note au format texte .txt)
    (MyPomodoro™) ..> (Démarrer le minuteur de la méthode Pomodoro)
    (MyPomodoro™) ..> (Arrêter le minuteur de la méthode Pomodoro)
}
```

---

## Base de données
```plantuml
class Connexion{
    id: VARCHAR(20) NOT NULL
    passwd: VARCHAR(20)
    PRIMARY KEY(id)
}
```

---

```sql
DROP DATABASE IF EXISTS myscol;
CREATE DATABASE myscol DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
use myscol ;

CREATE TABLE connexion(
id VARCHAR(20) NOT NULL ,
passwd VARCHAR(20) ,
PRIMARY KEY(id)
) ENGINE=Innodb;
```

---

## Accueil de notre site Web (Page de connexion)
Voici la page d'accueil de notre site Web qui est la page de connexion :<br>
![connexion1](Annexes/connexion1.png)<br><br>
Si l'identifiant ou le mot de passe est incorrect, il y a une fenêtre « Pop-up » qui apparaît pour dire que l'identifiant ou le mot de passe est incorrect et de réessayer :<br>
![connexion2](Annexes/connexion2.png)<br><br>
Une fois que la connexion a été réussie, nous pouvons désormais accéder aux trois applications Web (MyTodo™, MyNotes™ et MyPomodoro™) :<br>
![connexion3](Annexes/connexion3.png)<br><br>

---

## Présentation et utilisation de MyTodo™
MyTodo™ permet de gérer les tâches (écrire/ajouter/terminer/supprimer).<br>
Voici la page de MyTodo™ :<br>
![todo1](Annexes/todo1.png)<br><br>
S'il n'y a rien dans les champs de saisie de la tâche, il y a une fenêtre « Pop-up » qui apparaît pour dire d'écrire une tâche :<br>
![todo2](Annexes/todo2.png)<br><br>
Nous pouvons ajouter une tâche :<br>
![todo3](Annexes/todo3.png)<br><br>
Nous pouvons voir que les tâches ont été ajoutées dans la liste des tâches à effectuer :<br>
![todo4](Annexes/todo4.png)<br><br>
Nous pouvons terminer la tâche en cliquant dessus, puis la supprimer si on le souhaite :<br>
![todo5](Annexes/todo5.png)<br><br>

---

## Présentation et utilisation de MyNotes™
MyNotes™ permet d'écrire des notes et de les télécharger au format texte (.txt).<br>
Voici la page de MyNotes™ :<br>
![notes1](Annexes/notes1.png)<br><br>
Nous pouvons écrire le nom du fichier texte (.txt) lorsque nous allons télécharger la note, d'écrire une note et de la télécharger au format texte (.txt) :<br>
![notes2](Annexes/notes2.png)<br><br>
![notes3](Annexes/notes3.png)<br><br>
![notes4](Annexes/notes4.png)<br><br>

---

## Présentation et utilisation de MyPomodoro™
MyPomodoro™ permet de gérer son temps via la méthode Pomodoro (25 minutes de travail et 5 minutes de repos).<br>
Voici la page de MyPomodoro™ :<br>
![pomodoro1](Annexes/pomodoro1.png)<br><br>
Vous pouvez cliquer sur le bouton « Play » pour lancer le minuteur de 25 minutes pour travailler :<br>
![pomodoro2](Annexes/pomodoro2.png)<br><br>
![pomodoro3](Annexes/pomodoro3.png)<br><br>
Vous pouvez recommencer le minuteur si vous le souhaitez :<br>
![pomodoro4](Annexes/pomodoro4.png)<br><br>
Lorsque les 25 minutes ce sont écoulées en mode travail, le minuteur va basculer automatiquement en mode repos pour se reposer durant 5 minutes :<br>
![pomodoro5](Annexes/pomodoro5.png)<br><br>