Drop database if exists myscol;

CREATE DATABASE myscol DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
use myscol;

CREATE TABLE connexion(
id VARCHAR(20) NOT NULL,
passwd VARCHAR(20),
PRIMARY KEY(id)
) ENGINE=Innodb;